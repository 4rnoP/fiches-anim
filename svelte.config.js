import adapter from "@sveltejs/adapter-static"
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte"

import { readFileSync } from "node:fs"
import { fileURLToPath } from "node:url"
const path = fileURLToPath(new URL("package.json", import.meta.url))
const pkg = JSON.parse(readFileSync(path, "utf8"))

export default {
  preprocess: [vitePreprocess()],
  kit: {
    // adapter: adapter({
    // 	// default options are shown. On some platforms
    // 	// these options are set automatically — see below
    // 	pages: 'public',
    // 	assets: 'public',
    // 	fallback: null,
    // 	precompress: false,
    // 	strict: true
    // })
    version: {
      name: pkg.version
    },
    adapter: adapter()
  }
}
