# Fiches-anim

Un petit projet permettant de créer des fiches d'animation.
Une version de démo est disponible ici: https://fiches-anim.vercel.app/

Créé par Arno Pigeon sur une idée de Cyril Ryckebusch
