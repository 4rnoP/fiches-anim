import "pdfjs-dist/legacy/build/pdf.worker.mjs"
import { getDocument } from "pdfjs-dist"
import { jsPDF } from "jspdf"
import { PDFDocument } from "pdf-lib"

import type { FicheData } from "$lib/store"
import { openFile } from "$lib/utils"
import fiche from "$lib/store"

export async function openFiche(f: Event) {
  const file = await openFile(f).arrayBuffer()
  const doc = await PDFDocument.load(file)
  const docPdfJs = await getDocument(file).promise
  const newFiche = JSON.parse(<string>doc.getForm().getTextField("content").getText())
  console.log(newFiche)
  const attachements = await docPdfJs.getAttachments()
  newFiche.attachments = Object.values(attachements || {}).map((v) => {
    return {
      filename: (v as { filename: string }).filename,
      content: (v as { content: string }).content
    }
  })
  fiche.set(newFiche)
}

export async function saveFiche(fiche: FicheData) {
  const doc = new jsPDF()
  const print_div: HTMLElement = document.getElementById("print") as HTMLElement
  await doc.html(print_div as HTMLElement, {
    callback: async (doc) => {
      const pdfLibDoc = await PDFDocument.load(doc.output("arraybuffer"))
      pdfLibDoc
        .getForm()
        .createTextField("content")
        .setText(
          JSON.stringify(
            Object.keys(fiche)
              .filter((k) => k != "attachments")
              .reduce((acc, cur) => {
                return { ...acc, [cur]: fiche[cur as keyof typeof fiche] }
              }, {})
          )
        )

      for (const file of fiche.attachments) {
        pdfLibDoc.attach(file.content, file.filename).then(() => {})
      }
      pdfLibDoc.setTitle(fiche.title)
      pdfLibDoc.setSubject("Fiche d'animation")
      pdfLibDoc.setKeywords(
        (
          Object.entries(fiche.activityType)
            .flat(2)
            .filter((i) => i !== null) as string[]
        )
          .concat(fiche.cost || [])
          .concat(fiche.duration || [])
          .concat(Object.keys(fiche.participants))
          .concat(fiche.location)
      )
      //             Object.keys($fiche.activityType)
      //               .map(activity  => (activity === "Scientifique" && Boolean($fiche.activityType.Scientifique) && $fiche.activityType.Scientifique?.length != 0) ? $fiche.activityType.Scientifique?.join(", ") : activity)
      //               .map(activity => activity === "Autre" ? $fiche.activityType[activity] : activity)
      //               .map(str => str?.toLowerCase())
      //               .join(", ") || "non renseigné"
      pdfLibDoc.setAuthor(fiche.authors.join(", "))
      open(
        URL.createObjectURL(new Blob([await pdfLibDoc.save()], { type: "application/pdf" })),
        "pdf"
      )
    },
    margin: 20,
    width: 170,
    windowWidth: 800
  })
}
