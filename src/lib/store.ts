import { derived, get, writable } from "svelte/store"
import type { Readable, Writable } from "svelte/store"

export const ActivityList = [
  "Veillée",
  "Projet",
  "Grand jeu",
  "Petit jeu",
  "Défi",
  "Manuel",
  "Sportif",
  "Scientifique",
  "Artistique",
  "Calme",
  "Autre"
] as const

export const ActivityScienceList = [
  "Électronique",
  "Sciences sociales",
  "Mathématiques",
  "Biologie",
  "Chimie",
  "Physique",
  "Écologie",
  "SVT"
] as const

export type ActivityTypes = {
  [K in (typeof ActivityList)[number]]: K extends "Scientifique"
    ? (typeof ActivityScienceList)[number][]
    : K extends "Autre"
      ? string
      : null
}

export const PedagogyGoalList = [
  "Sensibiliser les participants",
  "Développer chez les participants",
  "Permettre aux participants",
  "Favoriser chez les participants"
] as const

export type PedagogyGoalsTypes = { [key in (typeof PedagogyGoalList)[number]]: string }

export const ParticipantList = [
  "Se vit seul",
  "Se vit en groupe",
  "Se vit en équipe",
  "Avec un minimum d'enfants requis"
] as const

export type ParticipantsTypes = {
  [K in (typeof ParticipantList)[number]]: K extends
    | "Se vit en groupe"
    | "Se vit en équipe"
    | "Avec un minimum d'enfants requis"
    ? number
    : null
}

export const DurationList = [
  "Une heure ou moins",
  "Une demi-journée",
  "Une journée",
  "Une semaine",
  "Non défini"
] as const

export const LocationList = [
  "En intérieur",
  "En extérieur dans l'endroit de camp",
  "En extérieur hors de l'endroit de camp"
] as const

export const CostList = [
  "Onéreux (> 5 €/enfant)",
  "À coût réduit",
  "Zéro déchet",
  "Sans matériel",
  "Avec le matériel de base"
]

export const MaterialMetricList = [
  "valeur absolue",
  "par animé",
  "par animé plus l'animateur",
  "par groupe d'animé",
  "par groupe d'animé plus l'animateur"
] as const

export type Material = {
  item: string
  quantity: number
  quantityUnit: string
  quantityMetric: (typeof MaterialMetricList)[number]
}

export type Attachment = {
  filename: string
  // type: string
  content: Uint8Array
}

export type FicheData = {
  title: string
  authors: Array<string>
  activityType: Partial<ActivityTypes>
  pedagogyGoals: Partial<PedagogyGoalsTypes>
  participants: Partial<ParticipantsTypes>
  duration?: (typeof DurationList)[number]
  location: Array<(typeof LocationList)[number]>
  cost?: (typeof CostList)[number]
  animators?: number
  min_age?: number
  materials: Array<Material>
  imaginary?: string
  goal: string
  methodology: string
  misc: string
  variants: string
  attachments: Array<Attachment>
}

export class ObjectStore<T extends object> implements Writable<object> {
  subscribe!: Readable<T>["subscribe"]
  set!: Writable<T>["set"]
  update!: Writable<T>["update"]

  static fromMethods<T2 extends object>(
    sub: Readable<T2>["subscribe"],
    set: Writable<T2>["set"],
    up: Writable<T2>["update"]
  ) {
    const s = new this<T2>()
    s.subscribe = sub
    s.set = set
    s.update = up
    return s
  }

  static fromStore<T2 extends object>(o: Writable<T2>) {
    const s = new this<T2>()
    s.subscribe = o.subscribe
    s.set = o.set
    s.update = o.update
    return s
  }

  static fromValue<T2 extends object>(value?: T2) {
    const { subscribe, set, update } = writable(value)
    return this.fromMethods<T2>(subscribe, set, update)
  }

  subStore<K extends keyof T>(key: K): ObjectStore<object & T[K]> {
    const r = derived(this, (s) => {
      return s[key]
    }) as unknown as Writable<T[K]>
    r.set = (value) =>
      this.update((f) => {
        f[key] = value
        return f
      })
    r.update = (fct) => r.set(fct(get(r)))
    return ObjectStore.fromStore(r as unknown as Writable<object & T[K]>)
  }

  fromKeys<V extends keyof T>() {
    const r = derived(this, (values) => Object.keys(values)) as Writable<V[]>
    r.set = (values) =>
      this.update((p) => Object.fromEntries(values.map((i) => [i, p[i] || null])) as T)
    r.update = (fn) => r.set(fn(get(r)))
    return r
  }
}

const fiche = ObjectStore.fromValue<FicheData>({
  imaginary: undefined,
  activityType: {},
  animators: undefined,
  authors: [],
  cost: undefined,
  duration: undefined,
  goal: "",
  location: [],
  materials: [],
  methodology: "",
  min_age: undefined,
  misc: "",
  participants: {},
  pedagogyGoals: {},
  title: "",
  variants: "",
  attachments: []
})

export default fiche
