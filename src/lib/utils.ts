export function TypedKeys<T extends object>(obj: T) {
  return Object.keys(obj) as Array<keyof T>
}

export function openFile(f: Event): File {
  return ((f.target as HTMLInputElement)?.files as FileList)[0]
}

export function getUniqueValues<T>(a: T[]): T[] {
  return a.filter((item, i, ar) => ar.indexOf(item) === i)
}
